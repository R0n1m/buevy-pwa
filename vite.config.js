import { VitePWA } from "vite-plugin-pwa";
import vue from "@vitejs/plugin-vue2";

module.exports = {
  plugins: [
    vue(),
    VitePWA({
      strategies: "injectManifest",
      srcDir: "src",
      filename: "sw.js",
      includeAssets: [
        "favicon.svg",
        "favicon.ico",
        "robots.txt",
        "apple-touch-icon.png",
      ],
      manifest: {
        name: "R0N1n-dev Vite PWA",
        short_name: "VVPWA",
        description: "Test for Vite PWA",
        theme_color: "#ffffff",
        icons: [
          {
            src: "icons/pwa-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "icons/pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
        ],
      },
    }),
  ],
};
