import Vue from "vue";
import { registerSW } from "virtual:pwa-register";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import "./assets/all.css";
import App from "./App.vue";

const updateSW = registerSW({
  onNeedRefresh() {},
  onOfflineReady() {},
});

Vue.use(Buefy);
new Vue({
  render: (h) => h(App),
}).$mount("#app");
