import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const router = new VueRouter({
    base: "/",
    mode: "history",
    routes: [
        { path: "/", component: Home },
        { path: "/about", component: () =>
                import ("../views/About") },
    ],
});

export default router;